import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ToastLibComponent } from 'toast-lib';
import { AuthInterceptors, ErrorInterceptors } from 'api-tmdb-lib';
@NgModule({
  declarations: [
    AppComponent,
    ToastLibComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AuthInterceptors,
    ErrorInterceptors
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
