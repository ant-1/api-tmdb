import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'movies', loadChildren: './external-wrapper/movies-ext-wrap.module#MoviesExtWrapModule' },
  { path: 'series', loadChildren: './external-wrapper/series-ext-wrap.module#SeriesExtWrapModule' },
  { path: 'auth', loadChildren: './external-wrapper/auth-ext-wrap.module#AuthExtWrapModule' },
  { path: '', redirectTo: '/movies', pathMatch: 'full' }
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  declarations: []
})
export class AppRoutingModule { }
