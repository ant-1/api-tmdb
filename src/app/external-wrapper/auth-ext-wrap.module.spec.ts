import { AuthExtWrapModule } from './auth-ext-wrap.module';

describe('AuthExtWrapModule', () => {
  let authExtWrapModule: AuthExtWrapModule;

  beforeEach(() => {
    authExtWrapModule = new AuthExtWrapModule();
  });

  it('should create an instance', () => {
    expect(authExtWrapModule).toBeTruthy();
  });
});
