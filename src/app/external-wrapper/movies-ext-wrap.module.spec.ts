import { MoviesExtWrapModule } from './movies-ext-wrap.module';

describe('MoviesExtWrapModule', () => {
  let moviesExtWrapModule: MoviesExtWrapModule;

  beforeEach(() => {
    moviesExtWrapModule = new MoviesExtWrapModule();
  });

  it('should create an instance', () => {
    expect(moviesExtWrapModule).toBeTruthy();
  });
});
