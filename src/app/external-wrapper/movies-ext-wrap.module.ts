import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MoviesModule, moviesRoutes } from 'movies-lib';

@NgModule({
  imports: [
    MoviesModule,
    RouterModule.forChild(moviesRoutes)
  ],
  declarations: []
})
export class MoviesExtWrapModule { }
