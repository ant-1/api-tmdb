import { SeriesExtWrapModule } from './series-ext-wrap.module';

describe('SeriesExtWrapModule', () => {
  let seriesExtWrapModule: SeriesExtWrapModule;

  beforeEach(() => {
    seriesExtWrapModule = new SeriesExtWrapModule();
  });

  it('should create an instance', () => {
    expect(seriesExtWrapModule).toBeTruthy();
  });
});
