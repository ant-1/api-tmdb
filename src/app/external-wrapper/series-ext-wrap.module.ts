import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SeriesModule, seriesRoutes } from 'series-lib';

@NgModule({
  imports: [
    SeriesModule,
    RouterModule.forChild(seriesRoutes)
  ],
  declarations: []
})
export class SeriesExtWrapModule { }
