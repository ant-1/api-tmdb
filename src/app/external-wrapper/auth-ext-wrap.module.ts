import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ApiTmdbLibModule, apiRoutes } from 'api-tmdb-lib';

@NgModule({
  imports: [
    ApiTmdbLibModule,
    RouterModule.forChild(apiRoutes)
  ],
  declarations: []
})
export class AuthExtWrapModule { }
