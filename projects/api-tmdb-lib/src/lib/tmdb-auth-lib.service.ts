import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TmdbAuthLibService {

  constructor(private http: HttpClient) { }

  // Get an Authorization Token from TMDb
  getAuthorizationToken(): Observable<any> {
    const url = 'https://api.themoviedb.org/4/auth/request_token';
    const redirect = {'redirect_to': 'http://localhost:4200/auth/logged'};
    return this.http.post<any>(url, redirect)
      .pipe(
        tap(res => console.log('res request token : ', res)),
        map(res => {
          return res;
        }),
        catchError(this.handleError<any[]>('getAuthorizationToken', []))
      );
  }

  // Get an Access Token from TMDb
  getAccessToken(requestToken): Observable<any> {
    const url = 'https://api.themoviedb.org/4/auth/access_token';
    const body = {'request_token': requestToken};
    return this.http.post<any>(url, body)
      .pipe(
        tap(res => console.log('res access token : ', res)),
        map(res => {
          return res;
        }),
        catchError(this.handleError<any[]>('getAccessToken', [])),
      );
  }

  // Delete the access token from TMDb
  deleteAccessToken(accessToken): Observable<any> {
    const body = {'access_token': accessToken};
    const url = `http://api.themoviedb.org/4/auth/access_token`;
    const httpOptions = {
      body
    };
    console.log('httpOptions : ', httpOptions);
    return this.http.request<any>('delete', url, httpOptions)
      .pipe(
        tap(res => {
          console.log('res delete token : ', res);
        }),
        map(res => {
          return res;
        }),
        catchError(this.handleError<any[]>('deleteAccessToken', [])),
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed.
   * @param result - optional value to return as the observable result.
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure.
      console.error(error); // log to console instead.

      // TODO: better job of transforming error for user consumption.
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
