import { ErrorHandlerModule } from './error-handler.module';

describe('ErrorHandlerModule', () => {
  let errorHandlerModule: ErrorHandlerModule;

  beforeEach(() => {
    errorHandlerModule = new ErrorHandlerModule();
  });

  it('should create an instance', () => {
    expect(errorHandlerModule).toBeTruthy();
  });
});
