import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { ToastLibService } from 'toast-lib';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
        private toastLibService: ToastLibService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(catchError((err) => {
            // intercept the respons error and displace it to the console
            console.log('intercept error : ', err);
            localStorage.removeItem('LSAccessToken');
            localStorage.removeItem('LSRequestToken');
            const error = err.error.message || err.statusText;
            this.toastLibService.show(error);
            return throwError(error);
        }));
    }
}
