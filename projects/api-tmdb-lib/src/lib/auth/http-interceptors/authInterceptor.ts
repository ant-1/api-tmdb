import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()

export class AuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const request = req.clone({
            headers: new HttpHeaders({
                'Content-Type':  'application/json;charset=utf-8',
                // tslint:disable-next-line:max-line-length
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjOTVlNWUzZjI5ZjhkYjhhMWZhNDgwMjUxOTI5ZDFkYSIsInN1YiI6IjU5Y2EwMzY4OTI1MTQxMjNiOTAwNTg5YSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.GGQrONco_2Frr5i2wyugsmN_NYWfexN5EDZEjS88ahw'
              })
        });

        return next.handle(request);
    }
}
