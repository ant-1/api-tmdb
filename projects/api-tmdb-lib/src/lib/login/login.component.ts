import { Component, OnInit } from '@angular/core';
import { TmdbAuthLibService } from '../tmdb-auth-lib.service';

@Component({
  selector: 'lib-login',
  template: '<p>Authentification</p>'
})
export class LoginComponent implements OnInit {

  constructor(
    private tmdbAuthLibService: TmdbAuthLibService
    ) { }

  ngOnInit() {
    this.login();
  }

  login(): void {
    if (!localStorage.getItem('lcRequestToken') && !localStorage.getItem('lcAccessToken')) {
      this.tmdbAuthLibService.getAuthorizationToken().subscribe(requestToken => {
        if (requestToken.success) {
          localStorage.setItem('lcRequestToken', JSON.stringify(requestToken));
          // tslint:disable-next-line:max-line-length
          const url = `https://www.themoviedb.org/auth/access?request_token=${JSON.parse(localStorage.getItem('lcRequestToken')).request_token}`;
          window.location.href = url;
        }
      });
    }
  }
}
