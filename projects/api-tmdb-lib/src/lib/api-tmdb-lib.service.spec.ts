import { TestBed } from '@angular/core/testing';

import { ApiTmdbLibService } from './api-tmdb-lib.service';

describe('ApiTmdbLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiTmdbLibService = TestBed.get(ApiTmdbLibService);
    expect(service).toBeTruthy();
  });
});
