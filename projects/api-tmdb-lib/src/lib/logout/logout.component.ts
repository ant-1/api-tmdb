import { Component, OnInit } from '@angular/core';
import { TmdbAuthLibService } from '../tmdb-auth-lib.service';

@Component({
  selector: 'lib-logout',
  template: '<p>Désauthentification</p>'
})
export class LogoutComponent implements OnInit {

  constructor(
    private tmdbAuthLibService: TmdbAuthLibService
    ) { }

  ngOnInit() {
    this.deleteAccessToken();
  }

  deleteAccessToken(): void {
    if (localStorage.getItem('lcAccessToken')) {
      this.tmdbAuthLibService.deleteAccessToken(JSON.parse(localStorage.getItem('lcAccessToken')).access_token)
      .subscribe();
      localStorage.removeItem('lcAccessToken');
    }
  }

}
