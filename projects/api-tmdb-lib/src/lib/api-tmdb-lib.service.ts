import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Actor } from './models/actor.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiTmdbLibService {

  private API_BASE = 'https://api.themoviedb.org/3'; // Base url of the api The Movie DB.
  private API_KEY = 'cfb2b338648587f6aabd21fd2342712b'; // KEY of the api The Movie DB.
  private imgUrl = 'http://image.tmdb.org/t/p/w500'; // base url of the images of The Movie DB.

  private urlKey = `api_key=${this.API_KEY}`;
  private lang = 'language=fr-FR';

  constructor(private http: HttpClient) { }
  // Get Popular from The Movie DB, based on the "i" page and the type (movie, tv).
  getPopular(i: number, type: string): Observable<any[]> {
    const page = `page=${i}`;
    const url = `${this.API_BASE}/${type}/popular?${this.urlKey}&${this.lang}&${page}`;

    return this.http.get<any>(url).pipe(
      map(res => res.results.map(item => {
        if (item) {
          item.poster_path = this.imgUrl + item.poster_path; // Replace the backdrop_path from TMDB to a usable path.
          item.backdrop_path = this.imgUrl + item.backdrop_path; // Replace the backdrop_path from TMDB to a usable path.
          return item;
        }
      })),
      catchError(this.handleError('getPopular', []))
    );
  }

  // Get item by id.
  getItem(id: number, type: string): Observable<any> {
    const url = `${this.API_BASE}/${type}/${id}?${this.urlKey}&${this.lang}`;
    return this.http.get<any>(url).pipe(
      map(item => {
        if (item) {
          item.poster_path = this.imgUrl + item.poster_path; // Replace the backdrop_path from TMDB to a usable path.
          item.backdrop_path = this.imgUrl + item.backdrop_path; // Replace the backdrop_path from TMDB to a usable path.

          return item;
        }
      }),
      catchError(this.handleError<any>(`getItem id=${id}`))
    );
  }

  // Get actors by id.
  getActors(id: number, type: string): Observable<Actor[]> {
    const url = `${this.API_BASE}/${type}/${id}/credits?${this.urlKey}`;

    return this.http.get<any>(url).pipe(
      map(res => res.cast.map(actor => {
        if (actor) {
          actor.profile_path = this.imgUrl + actor.profile_path; // Replace the backdrop_path from TMDB to a usable path.
          return actor;
        }
      })),
      catchError(this.handleError('getActors', []))
    );
  }

  // Get similar.
  getSimilar(id: number, i: number, type: string): Observable<any[]> {
    const page = `page=${i}`;
    const url = `${this.API_BASE}/${type}/${id}/similar?${this.urlKey}&${page}&${this.lang}`;

    return this.http.get<any>(url).pipe(
      map(res => res.results.map(item => {
        if (item) {
          item.poster_path = this.imgUrl + item.poster_path; // Replace the backdrop_path from TMDB to a usable path.
          item.backdrop_path = this.imgUrl + item.backdrop_path; // Replace the backdrop_path from TMDB to a usable path.
          return item;
        }
      })),
      catchError(this.handleError('getSimilar', []))
    );
  }

  // Get whose name contains search term.
  search(term: string, i: number, type: string): Observable<any[]> {
    const verb = 'search';
    const page = `page=${i}`;
    const url = `${this.API_BASE}/${verb}/${type}?${this.urlKey}&query=${term}&${this.lang}&${page}`;
    if (!term.trim()) {
      // if not search term, return empty array.
      return of([]);
    }
    return this.http.get<any>(url).pipe(
      map(res => res.results.map(item => {
        if (item) {
          item.poster_path = this.imgUrl + item.poster_path; // Replace the backdrop_path from TMDB to a usable path.
          item.backdrop_path = this.imgUrl + item.backdrop_path; // Replace the backdrop_path from TMDB to a usable path.
          return item;
        }
      })),
      catchError(this.handleError<any[]>('search', []))
    );
  }

  // Get an Authorization Token from TMDb
  getAuthorizationToken(): Observable<any> {
    const url = 'https://api.themoviedb.org/4/auth/request_token';
    const redirect = {'redirect_to': 'http://localhost:4200/auth/logged'};
    return this.http.post<any>(url, redirect)
      .pipe(
        tap(res => console.log('res request token : ', res)),
        map(res => {
          return res;
        }),
        catchError(this.handleError<any[]>('getAuthorizationToken', []))
      );
  }

  // Get an Access Token from TMDb
  getAccessToken(requestToken): Observable<any> {
    const url = 'https://api.themoviedb.org/4/auth/access_token';
    const body = {'request_token': requestToken};
    return this.http.post<any>(url, body)
      .pipe(
        tap(res => console.log('res access token : ', res)),
        map(res => {
          return res;
        }),
        catchError(this.handleError<any[]>('getAccessToken', [])),
      );
  }

  // Delete the access token from TMDb
  deleteAccessToken(accessToken): Observable<any> {
    const body = {'access_token': accessToken};
    const url = `http://api.themoviedb.org/4/auth/access_token`;
    const httpOptions = {
      body
    };
    console.log('httpOptions : ', httpOptions);
    return this.http.request<any>('delete', url, httpOptions)
      .pipe(
        tap(res => {
          console.log('res delete token : ', res);
          localStorage.removeItem('lcAccessToken');
        }),
        map(res => {
          return res;
        }),
        catchError(this.handleError<any[]>('deleteAccessToken', [])),
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed.
   * @param result - optional value to return as the observable result.
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure.
      console.error(error); // log to console instead.

      // TODO: better job of transforming error for user consumption.
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
