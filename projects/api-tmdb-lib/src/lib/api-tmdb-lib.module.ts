import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApiTmdbLibComponent } from './api-tmdb-lib.component';
import { LoginComponent } from './login/login.component';
import { LoggedComponent } from './logged/logged.component';
import { LogoutComponent } from './logout/logout.component';

export const apiRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logged', component: LoggedComponent },
  { path: 'logout', component: LogoutComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(apiRoutes)
  ],
  declarations: [
    ApiTmdbLibComponent,
    LoginComponent,
    LoggedComponent,
    LogoutComponent
  ],
  exports: [
    ApiTmdbLibComponent
  ]
})
export class ApiTmdbLibModule { }
