import { Component, OnInit } from '@angular/core';
import { TmdbAuthLibService } from '../tmdb-auth-lib.service';

@Component({
  selector: 'lib-logged',
  template: '<p>Authentifié</p>'
})
export class LoggedComponent implements OnInit {

  private accessToken: any;

  constructor(
    private tmdbAuthLibService: TmdbAuthLibService
    ) { }

  ngOnInit() {
    this.getAccessToken();
  }

  getAccessToken(): void {
    if (!localStorage.getItem('lcAccessToken')
        && localStorage.getItem('lcRequestToken')
        && JSON.parse(localStorage.getItem('lcRequestToken')).success)
    // tslint:disable-next-line:one-line
    {
      this.tmdbAuthLibService.getAccessToken(JSON.parse(localStorage.getItem('lcRequestToken')).request_token)
      .subscribe(accessToken => {
        this.accessToken = accessToken;
        if (this.accessToken.success) {
          localStorage.removeItem('lcRequestToken');
          localStorage.setItem('lcAccessToken', JSON.stringify(this.accessToken));
        }
      });
    }
  }

}
