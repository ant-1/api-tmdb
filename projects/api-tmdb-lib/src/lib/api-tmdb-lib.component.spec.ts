import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiTmdbLibComponent } from './api-tmdb-lib.component';

describe('ApiTmdbLibComponent', () => {
  let component: ApiTmdbLibComponent;
  let fixture: ComponentFixture<ApiTmdbLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiTmdbLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiTmdbLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
