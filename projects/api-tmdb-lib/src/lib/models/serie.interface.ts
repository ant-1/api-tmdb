// Movie model
export interface Serie {
    original_name: string;
    genre_ids: any;
    name: string;
    popularity: string;
    origin_country: string;
    vote_count: number;
    first_air_date: string;
    backdrop_path: string;
    original_language: string;
    id: number;
    vote_average: number;
    overview: string;
    poster_path: string;
    genres: any;
}
