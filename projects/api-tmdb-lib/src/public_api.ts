/*
 * Public API Surface of api-tmdb-lib
 */

export * from './lib/api-tmdb-lib.service';
export * from './lib/api-tmdb-lib.component';
export * from './lib/api-tmdb-lib.module';
export * from './lib/models/actor.interface';
export * from './lib/models/movie.interface';
export * from './lib/models/serie.interface';
export * from './lib/auth/http-interceptors/index';
export * from './lib/error-handler/http-interceptors/index';
